#!/bin/sh
# script should set up a docker image
# which can be used to develop for prairielearn
# in an environment that also has neo4j

[ -z "$1" ] && echo 'usage: ldl <docker-image>' && exit 0

echo "pulling pl submodule"
git submodule sync > /dev/null
git submodule update --init --recursive > /dev/null

echo "moving install script for neo4j to prairielearn source"
cp neo4j-install.sh ./prairielearn/tools/neo4j-install.sh

echo "install node packages to pl docker container"
docker run -w /PrairieLearn -v $(pwd)/prairielearn:/PrairieLearn prairielearn/prairielearn /usr/local/bin/npm ci
echo "install complete"

docker run -it --rm -p 3000:3000 -v $(pwd)/prairieLearn:/PrairieLearn prairielearn/prairielearn
